require("aurel.remap")
require("aurel.set")
require("aurel.packer")
vim.o.background = "dark" -- or "light" for light mode
vim.cmd([[colorscheme gruvbox-baby]])
